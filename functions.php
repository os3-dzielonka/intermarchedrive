<?php 

function csvtojsonorarr($file,$delimiter,$json)
{
    if (($handle = fopen($file, "r")) === false)
    {
            die("can't open the file.");
    }

    $csv_headers = fgetcsv($handle, 4000, $delimiter);
    $csv_json = array();

    while ($row = fgetcsv($handle, 4000, $delimiter))
    {
            $csv_json[] = array_combine($csv_headers, $row);
    }

    fclose($handle);
    if($json == true) {
    return json_encode($csv_json);
  } else {
    return $csv_json;
  }
}


$jsondriveresult = csvtojsonorarr("./statics/csv/drive_stores.csv", ",", true);
$jsonultraresult = csvtojsonorarr("./statics/csv/ultra_stores.csv", ",", true);

$driveresult = csvtojsonorarr("./statics/csv/drive_stores.csv", ",", false);
$ultraresult = csvtojsonorarr("./statics/csv/ultra_stores.csv", ",", false);

$mergedresult = array_merge($driveresult,$ultraresult);

$cityoption = array();

foreach ($mergedresult as $cityoption) {
$cityoptions[] = $cityoption['Miasto'];
}

$cityoptions = array_unique($cityoptions);

$collator = collator_create('pl-PL');
collator_sort($collator, $cityoptions);
?>