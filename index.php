<?php include_once('functions.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Drive Intermarche</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
</head>

<body>

    <?php
echo "<script>
    storesObj = {};
    storesObj.drive = $jsondriveresult;
    storesObj.ultra = $jsonultraresult;
    </script>";
?>

    <link rel="stylesheet" type="text/css" href="statics/css/main.css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <header class="header__main--top">
        <section class="header__main--top-sectioncol">
            <ul class="header__main--top-sectioncol-servicelist">
                <li><span class="header__main--top-icon-order"><img role="icon"
                            src="statics/img/header__main--top-icon-order.svg">&nbsp;Zamawiasz, kiedy Ci
                        wygodnie</span></li>
                <li><span class="header__main--top-icon-packaging"><img role="icon"
                            src="statics/img/header__main--top-icon-packaging.svg">&nbsp;My pakujemy za Ciebie</span>
                </li>
                <li><span class="header__main--top-icon-retrieve"><img role="icon"
                            src="statics/img/header__main--top-icon-retrieve.svg">&nbsp;Odbiór następnego dnia</span>
                </li>
            </ul>
        </section>

        <section class="header__main--top-sectioncol">
            <div role="banner" class="header__main--top-sectioncol-banner">
                <h1 class="header__main--top-sectioncol-title ezakupy"><span>E-zakupy</span>&nbsp;<img alt="Intermarche"
                        src="statics/img/header__main--top-sectioncol-logo-intermarche.svg">
                </h1>
            </div>
        </section>
        <div class="container-xl scrollTo">
            <section class="header__main--top-sectioncol">
                <h2 class="header__main--top-sectioncol-heading-h2"><span>1.</span>Wybierz sklep stacjonarny w swojej
                    okolicy</h2>
            </section>
            <section class="header__main--top-sectioncol">
                <nav>
                    <select id="header__main--top-select-city">
                        <option selected>Wybierz sklep</option><?php foreach ($cityoptions as $city) {
                        echo '<option value="' . $city . '">' . $city . '</option>';
                    }
                    ?>
                    </select><select id="header__main--top-select-street" style="display:none;"></select></nav>

            </section>
            </nav>
        </div>
    </header>
    <main class="main__content--buttonbox">
        <section class="main__content--buttonbox-sectioncol" style="display: none">
            <div class="relative">
                <div class="container-xl">
                    <h2 class="header__main--top-sectioncol-heading-h2 diff"><span>2.</span>Przejdź do usługi, którą oferuje
                        Twój
                        sklep</h2>
                </div>

                <div class="container-xl">
                    <img id="store-type-logo" src="statics/img/intermarche_drive_logo.svg" class="logo-left center" />
                    <ul class="header__main--top-sectioncol-servicelist-wrap">
                        <li><span class="header__main--top-icon-packaging"><img role="icon"
                                    src="statics/img/body__content--icons-05.svg">&nbsp;Złóż zamówienie
                                wygodnie</span></li>
                        <li><span class="header__main--top-icon-packaging change"><img role="icon"
                                    src="statics/img/body__content--icons-06.svg">&nbsp;<span class="dif">Zapłać online lub na
                                miejscu</span></span>
                        </li>
                        <li><span class="header__main--top-icon-retrieve"><img role="icon"
                                    src="statics/img/body__content--icons-07.svg">&nbsp;Odbierz zakupy i ciesz się
                                zaoszczędzonym czasem</span></li>
                    </ul>
                </div>
<!--                <div class="mob-spacer"></div>-->
<!--                <div class="main__content--buttonbox-female-groceries"></div>-->
                <div class="container-xl">
                    <a target="_blank" id="action-button" class="main__content--button mb-neg-m" href="#">Zrób
                        zakupy</a>
                </div>
            </div>
        </section>
    </main>

    <section class="full backIMG">
        <span class="abs">
            DLACZEGO<br>
            WARTO KORZYSTAĆ<br>
            Z e-zakupów?
        </span>
        <div class="half deskt">
            <div class="box">
            </div>
            <div class="box black">
                <span class="small">
                    Robiąc e-zakupy masz pewność,
                    że produkty, które zamawiasz są
                    zawsze świeże. Nie musisz wybierać
                    owoców, ryb, warzyw, pieczywa,
                    bo zrobimy to za Ciebie.
                </span>
            </div>
            <div class="box meat">
                <span class="medium">
                    sprawdzona<br>
                    jakość
                </span>
            </div>
            <div class="box black">
                <span class="medium">
                    zakupy<br>
                    w dogodnym<br>
                    czasie
                </span>
            </div>

            <div class="box large bread">
                <span class="extraLarge">
                    oszczędność<br>
                    czasu
                </span>
            </div>
            <div class="box white">
                <span class="medium black">
                    brak<br>
                    kolejek
                </span>
            </div>
            <div class="box veggies">
                <span class="medium">
                    kompleksowa<br>
                    obsługa
                </span>
            </div>
            <div class="box white">
                <span class="medium black">
                    Kupujesz<br>
                    bezpiecznie
                </span>
            </div>
        </div>
    </section>

    <section class="full bigger mob">
        <div class="half">
            <div class="box">
            </div>
            <div class="box black">
                <span class="small">
                    Robiąc e-zakupy masz pewność,
                    że produkty, które zamawiasz są
                    zawsze świeże. Nie musisz wybierać
                    owoców, ryb, warzyw, pieczywa,
                    bo zrobimy to za Ciebie.
                </span>
            </div>
            <div class="box meat">
                <span class="medium">
                    sprawdzona<br>
                    jakość
                </span>
            </div>
            <div class="box black">
                <span class="medium">
                    zakupy<br>
                    w dogodnym<br>
                    czasie
                </span>
            </div>

            <div class="box large bread">
                <span class="extraLarge">
                    oszczędność<br>
                    czasu
                </span>
            </div>
            <div class="box white">
                <span class="medium black">
                    brak<br>
                    kolejek
                </span>
            </div>
            <div class="box veggies">
                <span class="medium">
                    kompleksowa<br>
                    obsługa
                </span>
            </div>
            <div class="box white">
                <span class="medium black">
                    Kupujesz<br>
                    bezpiecznie
                </span>
            </div>
        </div>
    </section>

    <section class="main__content--buttonbox-sectioncol groceries">
<!--        <ul class="header__main--top-sectioncol-servicelist-wrap vertical">-->
<!--            <li><span class="header__main--top-icon-packaging"><img role="icon"-->
<!--                        src="statics/img/body__content--icons-08.svg">&nbsp;Oszczędność czasu</span>-->
<!--            </li>-->
<!--            <li><span class="header__main--top-icon-packaging"><img role="icon"-->
<!--                        src="statics/img/body__content--icons-09.svg">&nbsp;Kupujesz bezpiecznie</span>-->
<!--            </li>-->
<!--            <li><span class="header__main--top-icon-wallet"><img role="icon"-->
<!--                        src="statics/img/body__content--icons-04.svg">&nbsp;Ceny jak w sklepie.<br>Nie-->
<!--                    przepłacasz</span></li>-->
<!---->
<!---->
<!--        </ul>-->
        <div class="container-m">
            <h2 class="header__main--top-sectioncol-title text-left mt-m ml-s">
                Dlaczego <br>warto korzystać<br>
                z&nbsp;e-zakupów?</h2>
        </div>
        <h3 class="header__main--top-sectioncol-title text-left grey-block smaller ml-a mb-neg-m diff">
            <span>Oszczędność <br>
                czasu</span>
        </h3>
    </section>

    <section class="main__content--buttonbox-sectioncol bread">
        <div class="container-m">
            <p class="header__main--top-sectioncol-title text-left grey-block-invert bigger mt-neg-xxl"><span>Robiąc
                    e-zakupy masz pewność,<br>
                    że produkty, które zamawiasz są<br>
                    zawsze świeże. Nie musisz wybierać<br>
                    owoców, ryb, warzyw, pieczywa,<br>
                    bo zrobimy to za Ciebie.</span></p>

            <p class="header__main--top-sectioncol-title text-left grey-block-invert smaller m-special"><span>
                    Warzywa, owoce i nabiał<br>
                    od wyselekcjonowanych <br>
                    dostawców, mięso z własnego <br>
                    wyrobu i wędzarni <br>
                    oraz pieczywo wypiekane <br>
                    na bieżąco - to główne <br>
                    atuty Intermache
                </span></p>
        </div>
    </section>
    <section class="main__content--buttonbox-sectioncol veggies">
        <div class="container-m">
            <h3 class="header__main--top-sectioncol-title text-left grey-block bigger ml-a mt-neg-m diff2">
                <span>Zakupy <br>w dogodnym <br>czasie
                </span>
            </h3>

            <h3 class="header__main--top-sectioncol-title text-left grey-block smaller2 ml-a"><span>Brak <br>
                    kolejek</span>
            </h3>

            <h3 class="header__main--top-sectioncol-title text-left grey-block smaller3 "><span>Wszystko <br>w jednym
                    <br>miejscu</span>
            </h3>
        </div>
    </section>
    <section class="main__content--buttonbox-sectioncol meat">
        <div class="container-m">
            <h3 class="header__main--top-sectioncol-title text-left grey-block bigger ml-a m-special-invert diffR">
                <span>Kompleksowa <br>
                    obsługa</span>
            </h3>
        </div>
    </section>
    <script src="statics/js/script.js"></script>
</body>

</html>