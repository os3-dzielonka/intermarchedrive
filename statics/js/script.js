cities = storesObj.ultra.concat(storesObj.drive);

var cityunique = [];
var citydistinct = [];

for (let i = 0; i < cities.length; i++) {
  if (!cityunique[cities[i].Miasto]) {
    citydistinct.push(cities[i].Miasto);
    cityunique[cities[i].Miasto] = 1;
  }
}

var curr_selection = $("#header__main--top-select-city").val();
var streets_curr = cities.filter((item) => item.Miasto === curr_selection);

$(function () {
  $(window).resize(function() {
    hideShow();
  });

  function hideShow(){
    if ($(window).width() < 991) {
      $('.deskt').hide();
      $('.mob').show();
    }
    else {
      $('.deskt').show();
      $('.mob').hide();
    }
  }
  hideShow();
  $("#header__main--top-select-street").html("");

  var iterateThroughStreets = function () {
    $.each(streets_curr, function (i, item) {
      if (item.Typ === "Drive") {
        $("#action-button").text("Zrób zakupy");
        $("#store-type-logo").attr(
          "src",
          "statics/img/intermarche_drive_logo.svg"
        );
        $(".change img").attr(
            "src",
            "statics/img/body__content--icons-06.svg"
        );
        $(".change .dif").text("Zapłać online lub na miejscu");


      } else {
        $("#action-button").text("Zarezerwuj zakupy");
        $("#store-type-logo").attr(
          "src",
          "statics/img/intermarche_ultra_logo.png"
        );
        $(".change img").attr(
            "src",
            "statics/img/walet.png"
        );
        $(".change .dif").text("Dokonaj płatności na miejscu w sklepie");
      }
      if (item.Adres) {
        $("#header__main--top-select-street")
          .append(
            $("<option>", {
              value: item.Adres,
              text: item.Adres,
            })
          )
          .slideDown();
      } else {
        $("#header__main--top-select-street").slideUp();
        $("#header__main--top-select-street").html("");
      }
    });
  };

  iterateThroughStreets();

  var iterateThroughUrls = function () {
    $.each(streets_curr, function (i, item) {
      if (item.AdresURL) {
        $(".main__content--buttonbox-sectioncol .main__content--button").attr(
          "href",
          item.AdresURL
        );
        $(".main__content--buttonbox-sectioncol").slideDown();
      } else {
        $(".main__content--buttonbox-sectioncol").slideUp();
      }
    });
  };

  iterateThroughUrls();

  $("#header__main--top-select-city").on("change", function () {
    curr_selection = $(this).val();
    streets_curr = cities.filter((item) => item.Miasto === curr_selection);

    $("#header__main--top-select-street").html("");
    iterateThroughStreets();
    iterateThroughUrls();

    $('html, body').animate({
      scrollTop: $(".scrollTo").offset().top
    }, 600);
  });

  $("#header__main--top-select-street").on("change", function () {
    curr_selection = $(this).val();
    urls_curr = cities.filter((item) => item.Miasto === curr_selection);

    $("#main__content--button").attr("href", curr_selection);
  });



});
